// console.log("Hello World");

// Repetition Control Structure (Loops)
	// Loops are one of the most important feature that programming must have.
	// It lets us execute code repeatedly in a pre-set number or maybe forever.


function greeting(){
	console.log("Hello My World!");
	
}

// greeting();
// greeting();
// greeting();

// let count = 10;

// while(count !==0){
// 	console.log("This is printed inside the loop: " + count);
// 	greeting();
// 	count--;

// 	// iteration
// }


// [SECTION] While Loop
	// While loop allow us to repeat an action or an instruction as long as the condition is true.
	/*
		Syntax:
			while(expression/condition){
				//statement/code block

				finalExpression ++/-- (iteration)
			}


			- expression/condition => This are the unit of code that is being evaluated in our loop. Loop will run while the condition/expression is true
			- statement/code block => code/instructions that will be executed several times.
			- finalExpression => indicates how to advance the loop.
	
	*/

	// let count = 5;

	// // while the value of count is not equal to 0.
	// while(count !== 0){

	// 	console.log("Current value of count in the condition: " + count + " !== 0 is " + (count !== 0))
	// 	// console.log("While: " + count);

	// 	// Decreases the value of count by 1 after every iteration of our loop to stop it when it reaches to 0.
	// 	// Forgetting to include this in loop will make our application run an infinite loop which will eventually crash our devices.
	// 	count--
	// }


// [SECTION] Do While Loop
	// A do-while loop works a lot like the while loop. But unlike while loops do-while lopps guarantee that the code will be executed at least once.

	/*
		Syntax:
			do {
				//statement/code block

				finalExpression ++/--
			} while(expression/condition)

	*/
				// Number() can also convert a boolean or date.
	// let number = Number(prompt("Give me a number"));

	// do {
	// 	console.log("Do While: " + number);

	// 	//increase the value of number by 1 after every iteration to stop when reaches 10 or greater.
	// 	number += 1;
	// 	//Providing a number of 10 or greater will run the code block once and will stop the loop
	// } while (number < 10);

	// while (number < 10){
	// 	console.log("While: " + number);

	// 	number++
	// }



// [SECTION] For Loop
	// A for loop is more flexible than while and do-while loops.

	/*
		Syntax:
			for (initialization; expression/condition; finalExpression/iteration ++/--){
				//statement/code block
			}

			Three parts of the for loop:
			1. initialization => value that will track the progress of the loop.
			2. expression/condition => This will be evaluated to determine if the loop will run one more time.
			3. finalExpression/iteration => indicates how the loop advances. ++/--
	
	*/

	/*
		Mini Activity

			Refractor the code below that the loop will only print the even number.



	*/

	// for (let count = 0; count <= 20; count++){
	// 	if (count % 2 == 0 && count !== 0){
	// 		console.log("For Loop: "  + count);
	// 				}

	// 	// console.log("For Loop: " + count);

	// }




	/*
		let myString = "geward";

		expected output:
		g
		e
		w
		a
		r
		d



	*/

	// Loop using String property

	let myString = "geward";

	// .length property is used to count the number of characters in a string.
	// console.log(myString.length); //result is 6 - have a number value

	// Accessing element of a string
	// Individual characters of a string may be accessed using it's index number.
	// The first character in string corresponds to the number 0, to the nth number.
	// console.log(myString[0]); //g - to access the starting element
	// console.log(myString[3]); //a
	// console.log(myString[myString.length-1]); //d - to access the last element of the string


	// Create the loop that will display the individual letters of the myString variable in the console.

	// for (let x = 0; x < myString.length; x++){
	// 	console.log(myString[x]);
	// }


	/*
		Creates a loop  that will print out the letters of the name individually and print out the number "3" instead when the letter to be printed out is a vowel.


	*/

	// let myName = "Geward"; // expected output: g 3 w 3 r d
	// let myNewName = "";
	// for (let i = 0; i < myName.length; i++){
	// 	// console.log(myName[i].toLowerCase());

	// 	// If the character of your name is a vowel letter, instead of the character, it will display a number "3".
	// 	if (myName[i].toLowerCase() == 'a' ||
	// 		myName[i].toLowerCase() == 'e' ||
	// 		myName[i].toLowerCase() == 'i' ||
	// 		myName[i].toLowerCase() == 'o' ||
	// 		myName[i].toLowerCase() == 'u'){

	// 		//vowels
	// 		console.log(3);
	// 		myNewName += 3;
	// 	}
	// 	else {
	// 		// consonants
	// 		console.log(myName[i]);
	// 		myNewName += myName[i];
	// 	}
	// }

	// concatenates the characters based on the given condition
	// console.log(myNewName);



// [SECTION] Continue and Break Statments
/*
	- "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the code block.
	- "break" statement is used to terminate the current loop once a match has been found.

*/

/*
	Create a loop that if the count values is divided by 2 and the remainder is 0, it will not print the number and continue to the next iteration of the loop. If the count value is greater than 10 it will stop the loop.


*/

	// for (let count = 0; count <= 20; count++){
	// 	// console.log(count);

	// 	if (count % 2 === 0){
	// 		continue;
	// 		// This ignores all statements located after the continue statement

	// 		// console.log("Even: " + count);
	// 	}
	
	// 		console.log("Continue and Break: " + count);
		
	// 	if (count > 10){
	// 		break;
	// 	}
	// }






	/*
		Mini Activity:
		Creates a loop that will iterate based on the length of the string. If the current letter is equivalent to "a", print "Continue to the next iteration" and skip the current letter. If the current letter is equal to "d" stop the loop.

		let name = "Alexandro";


		Expected Output:

		Continue to the next iteration
		l
		e
		x
		Continue to the next iteration
		n
		d




	*/

let name = "Alexandro";

for (let i = 0; i < name.length; i++){
	if (name[i].toLowerCase() == "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	else if (name[i].toLowerCase() == "d"){
		console.log(name[i]);
		break;
	}
	console.log(name[i]);

}